@extends('app.layouts.app')

@section('content')
    <div class="body-wrap">
        <header class="site-header">
            <div class="container">
                <div class="site-header-inner">
                    <div class="brand header-brand">
{{--                        <h1 class="m-0">--}}
{{--                            <a href="/">--}}
{{--                                <img class="header-logo-image" src="/images/logo.svg" alt="Logo">--}}
{{--                            </a>--}}
{{--                        </h1>--}}
                    </div>
                </div>
            </div>
        </header>

        <main>
            <section class="hero">
                <div class="container">
                    <div class="hero-inner">
                        <div class="hero-copy">
                            <h1 class="hero-title mt-0 lfm-font">
                                LIFE FOR MEMORY <br>
                                DREAMER
                            </h1>
                            <p class="hero-paragraph">
                                Новый альбом. Инструментал. Один трек с вокалам в акустическом варианте. Еще один из треков был в короткометражке и старые добрые синглы, которые теперь под одним названием DREAMER.
                            </p>
                            <!--	                        <div class="hero-cta">-->
                            <!--								<a class="button button-primary" href="#">-->
                            <!--									Go to Spotify-->
                            <!--								</a>-->
                            <!--								<a class="button" href="#">-->
                            <!--									Go to VK-->
                            <!--								</a>-->
                            <!--							</div>-->
                        </div>
                        <div class="hero-figure anime-element">
                            <svg class="placeholder" width="528" height="396" viewBox="0 0 528 396">
                                <rect width="528" height="396" style="fill:transparent;"/>
                            </svg>
                            <div class="hero-figure-box hero-figure-box-01" data-rotation="45deg"></div>
                            <div class="hero-figure-box hero-figure-box-02" data-rotation="-45deg"></div>
                            <div class="hero-figure-box hero-figure-box-03" data-rotation="0deg"></div>
                            <div class="hero-figure-box hero-figure-box-04" data-rotation="-135deg"></div>
                            <div class="hero-figure-box hero-figure-box-05" style="background-image: url('/images/dreamer/cover.jpg'); background-size: 100%;"></div>
                            <!--							<div class="hero-figure-box hero-figure-box-06" style="background-image: url('/dist/images/dreamer/cover.jpg'); background-size: 100%;"></div>-->
                            <!--							<div class="hero-figure-box hero-figure-box-07" style="background-image: url('/dist/images/dreamer/cover.jpg'); background-size: 100%;"></div>-->
                            <!--							<div class="hero-figure-box hero-figure-box-08" data-rotation="-22deg"></div>-->
                            <div class="hero-figure-box hero-figure-box-09" data-rotation="-52deg"></div>
                            <div class="hero-figure-box hero-figure-box-10" data-rotation="-50deg"></div>
                            <div class="logo">
                                <img src="/images/dreamer/cover.jpg" alt="logo">
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </main>

        <footer class="site-footer">
            <div class="container">
                <div class="site-footer-inner">
{{--                    <div class="brand footer-brand">--}}
{{--                        <a href="#">--}}
{{--                            <img class="header-logo-image" src="/images/logo.svg" alt="Logo">--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                    <ul class="footer-links list-reset">--}}
{{--                        <li>--}}
{{--                            <a href="#">Contact</a>--}}
{{--                        </li>--}}
{{--                        <li>--}}
{{--                            <a href="#">About us</a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
                    <ul class="footer-social-links list-reset">
                        <li>
                            <a href="https://www.instagram.com/lifeformemory/" target="_blank">
                                <span class="screen-reader-text">Instagram</span>
                                <img src="/images/social/instagram4.svg" alt="instagram" srcset="/images/social/instagram4.svg" width="16" height="16">
                            </a>
                        </li>
                        <li>
                            <a href="https://www.youtube.com/channel/UCsJBWmy5rhQW8nU1f1Nah3g" target="_blank">
                                <span class="screen-reader-text">YouTube</span>
                                <img src="/images/social/youtube.svg" alt="youtube" srcset="/images/social/youtube.svg" width="16" height="16">
                            </a>
                        </li>
                        <li>
                            <a href="https://open.spotify.com/album/4HJUtNCQbuTLOLWygRpYzI?si=P9oN7y3NRMiklvivBhgvGQ" target="_blank">
                                <span class="screen-reader-text">Spotify</span>
                                <img src="/images/social/spotify.svg" alt="spotify" srcset="/images/social/spotify.svg" width="16" height="16">
                            </a>
                        </li>
                        <li>
                            <a href="https://vk.com/lifeformemory" target="_blank">
                                <span class="screen-reader-text">VK</span>
                                <img src="/images/social/vk.svg" alt="vk" srcset="/images/social/vk.svg" width="16" height="16">
                            </a>
                        </li>
                        <li>
                            <a href="https://soundcloud.com/lifeformemory" target="_blank">
                                <span class="screen-reader-text">Soundcloud</span>
                                <img src="/images/social/soundcloud.svg" alt="soundcloud" srcset="/images/social/soundcloud.svg" width="16" height="16">
                            </a>
                        </li>
{{--                        <li>--}}
{{--                            <a href="#">--}}
{{--                                <span class="screen-reader-text">Sound Cloud</span>--}}
{{--                                <img src="/images/social/soundcloud.svg" alt="instagram" srcset="/images/social/soundcloud.svg" width="16" height="16">--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>
                    <div class="footer-copyright">&copy; 2021 Life for memory, all rights reserved</div>
                </div>
            </div>
        </footer>
    </div>
@endsection
